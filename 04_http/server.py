from flask import Flask, jsonify, request
import json

app = Flask(__name__)

liste_eleves = ['Pierre', 'Paul', 'Jacques']

@app.route('/obtenir/liste', methods=['GET'])
def obtenir_liste():
    return jsonify(liste_eleves)

@app.route('/bonjour/<nom>', methods=['GET'])
def bonjour(nom):
    return jsonify(f'bonjour {nom}!!!')

@app.route('/ajouter/eleve', methods = ['POST'])
def ajouter_eleve():
    nouvel_eleve = json.loads(request.data.decode('utf8'))
    print("nouvel élève ajouté: ",nouvel_eleve)
    liste_eleves.append(nouvel_eleve['nom'])
    return jsonify('eléve ajouté avec succès')

@app.route('/ajouter/eleve/<eleve>', methods = ['GET'])
def ajouter_eleve_get(eleve):
    liste_eleves.append(eleve)
    return jsonify('eléve ajouté avec succès par une méthode GET')


app.run(host='0.0.0.0', port='5000', debug=True)