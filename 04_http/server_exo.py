from flask import Flask, jsonify, request
import json

app = Flask(__name__)

utilisateurs = [
    {
        'nom': 'Pierre',
        'password': 'azerty'
    },
    {
        'nom': 'Paul',
        'password': '12345678'
    },
    {
        'nom': 'Jacques',
        'password': 'Jacques'
    }
]

def noms_utilisateurs():
    return [utilisateur['nom'] for utilisateur in utilisateurs]

def passwords_utilisateurs():
    return [utilisateur['password'] for utilisateur in utilisateurs]

def password_utilisateur(nom):
    res = "utilisateur inconnu"
    for utilisateur in utilisateurs:
        if nom == utilisateur['nom']:
            res = utilisateur['password']
            break  #interompt la boucle, cela n'est pas obligatoire
    return res

@app.route('/index.html', methods = ['GET'])
def index():
    return jsonify('Bienvenue')


@app.route('/users', methods = ['GET'])
def users():
    return jsonify(utilisateurs)

app.run(host='0.0.0.0', port='5000', debug=True)