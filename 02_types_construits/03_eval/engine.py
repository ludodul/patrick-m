
import random as rd


seed = rd.randint(0,100)


nbr_exos = 11
list_score = [0 for _ in range(nbr_exos)]

score_max = [1,1,1,1,1,2,2,2,3,3,3]

def exo2():
    rd.seed(seed)
    liste = ['lorem', 'ipsum', 'dolor', 'sit', 'amet,', 'consectetur', 'adipiscing', 'elit,', 'sed', 'do', 'eiusmod', 'tempor', 'incididunt', 'ut', 'labore', 'et', 'dolore', 'magna', 'aliqua.', 'ut', 'enim', 'ad', 'minim', 'veniam,', 'quis', 'nostrud', 'exercitation', 'ullamco', 'laboris', 'nisi', 'ut', 'aliquip', 'ex', 'ea', 'commodo', 'consequat.', 'duis', 'aute', 'irure', 'dolor', 'in', 'reprehenderit', 'in', 'voluptate', 'velit', 'esse', 'cillum', 'dolore', 'eu', 'fugiat', 'nulla', 'pariatur.', 'excepteur', 'sint', 'occaecat', 'cupidatat', 'non', 'proident,', 'sunt', 'in', 'culpa', 'qui', 'officia', 'deserunt', 'mollit', 'anim', 'id', 'est', 'laborum.']
    indice = rd.randint(0,len(liste)-1)
    mot = liste[rd.randint(0,len(liste)-1)]
    print('                 ')
    print("Question 1")
    print("Ecrire et éxécuter l'instruction permettant de trier cette liste par ordre alphabétique sans en changer le nom.")
    print('       ')
    print("Question 2")
    print(f"Ecrire et exécuter l'instruction permettant d'afficher le mot d'indice {indice} dans la liste triée.")
    print('      ')   
    print("Question 3")
    print(f"Ecrire et exécuter l'instruction permettant d'afficher l'indice du premier mot '{mot}' dans liste triée.")




        
def exo4(fonc):
    num_exo = 4
    rd.seed(seed)     
    test = [ rd.randint(-100,100) for i in range(10) ]
    res = fonc(test)
    try:
        for i in range(len(test)):
            assert res[i] == test[i]+1
        list_score[num_exo-1] = 2
        print('Bravo!!!')
    except:
        print('Réponse fausse... Essayez encore...')
        
        
def exo5(fonc):
    num_exo = 5
    
    assert seed != 0, "veuillez vous identifier"
    rd.seed(seed+4)     
    test = "Attention, (ceci est le test), il y a aeiouy et aussi AEIOUY et même àéèù et des signes de ponctuation ,;:!?.()"

    #fonction solution
    def sol(chaine) :
        dico = {'consonne': 0,
                'voyelle': 0,
                'espace': 0,
                'ponctuation':0
                }
        for car in chaine :
            if car in ['a','A','e','E','i','I','o','O','u','U','y','Y','à','é','è','ù'] :
                dico['voyelle'] += 1
            elif car in ['.',',',';',':','!','?','(',')'] :
                dico['ponctuation'] += 1
            elif car == ' ' :
                dico["espace"] += 1
            else :
                dico["consonne"] += 1
        
        return dico
    
    res = sol(test)

    try:
    
        assert fonc(test) == res
        list_score[num_exo-1] = 2
        print('Bravo!!!')
    except:
        print('Réponse fausse... Essayez encore...')
    

        
       
def exo7(fonc):
    num_exo = 7
    rd.seed(seed+6)
    q1 = rd.randint(1,15)
    q2 = rd.randint(1,15)
    q3 = rd.randint(1,15)
    q4 = rd.randint(1,15) 
    p1 = round(rd.random()*2,2)
    p2 = round(rd.random()*2,2)
    p3 = round(rd.random()*2,2)
    p4 = round(rd.random()*2,2)
    test = {
            'carotte': {
                'quantite': q1,
                'prix_unitaire': p1 
            },
            'choux': {
                'quantite': q2,
                'prix_unitaire': p2
            },
            'farine': {
                'quantite': q3,
                'prix_unitaire': p3
            },
            'tomate': {
                'quantite': q4,
                'prix_unitaire': p4
            }
        }
    res = p1*q1 + p2*q2 + p3*q3 + p4*q4
    try:
        assert fonc(test) == res
        list_score[num_exo-1] = 2
        print('Bravo!!!')
    except:
        print('Réponse fausse... Essayez encore...')
        
    
        
def exo8(fonc):
    num_exo = 8
    rd.seed(seed+7)     
    test = [[rd.randint(1,20) for i in range(rd.randint(5,10))] for j in range(10)]
    res = [sum(i)/len(i) for i in test]
    try:
        for i in range(len(test)):
            assert res[i] == fonc(test[i])
        list_score[num_exo-1] = 2
        print('Bravo!!!')
    except:
        print('Réponse fausse... Essayez encore...')
    
        
        
def exo9(fonc):
    num_exo = 9

    rd.seed(seed+8)     
    test = [[rd.randint(1,20) for i in range(rd.randint(5,10))] for j in range(10)]
    my = [sum(i)/len(i) for i in test]
    res = [fonc(l) for l in test]
    try:
        for i in range(len(test)):
            assert (my[i]>10) == res[i]
        list_score[num_exo-1] = 2
        print('Bravo!!!')
    except:
        print('Réponse fausse... Essayez encore...')
        
        
def exo11(fonc):
    
    num_exo = 11

    rd.seed(seed+10)     
    test = [9,2020,1983,9999,18949816]
    res = [1,0,3,1,4]
    try:
        for i in range(len(test)):
            assert res[i] == fonc(test[i])
        list_score[num_exo-1] = 3
    except:
        pass
        
    
   



        






    
    

prenoms = []
f = open('Prenoms.csv',encoding='latin-1')
for line in f.readlines()[1:]:
    prenoms.append(line.split(';')[0])
    
def alea_pass(size):
    caracteres = "azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN0123456789" #Tu peux en ajouter si tu veux
    longueur = size 
    mdp = "" 
    compteur = 0 

    while compteur < longueur:
        lettre = caracteres[rd.randint(0, len(caracteres)-1)] #On tire au hasard une lettre
        mdp += lettre #On ajoute la lettre au mot de passe
        compteur += 1 #On incrémente le compteur de lettres
    return mdp

    
liste_users = [{'identifiant': rd.choice(prenoms), 'password':alea_pass(rd.randint(4,10)) } for i in range(500)] 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    