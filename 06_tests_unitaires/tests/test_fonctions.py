from fonctions import *

def test_mult_3_01():
    assert mult_3(3) == 9
    assert mult_3(5) == 15

def test_mult_3_02():
    assert mult_3('toto') == 'totototototo'
    
    